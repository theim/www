---
title: "Reference Package"
date: 2021-04-20T09:35:19-07:00
draft: false
---

# Reference package
This is a collection typical material I give to people new to the project (typically students) as an introduction or as a reference for their project.

Some might require CERN or LBL login, if you don't have the right login contact me.

--------------------------------------

## General

- New to particle physics:
    - [ATLAS website](https://atlas.cern/discover/detector)
    - [Particle adventure](https://particleadventure.org/index.html)
    - [Silicon Detectors: Principles and Technology - Carl Haber](http://physics.unm.edu/pandaweb/events/uploads/haber_silicon.pdf)
- ATLAS Tracker upgrade:
    - [Status of the Inner Tracker Upgrade - Timon Heim](https://drive.google.com/file/d/1eQd7-_dkfwPC8dESCIuLL1xWPOGTEVbu/view?usp=sharing)
- Berkeley Lab stuff:
    - [Berkeley Lab Gitlab repo](https://gitlab.cern.ch/berkeleylab)
    - [Berkeley Lab wiki](https://atlaswiki.lbl.gov/strips/index)

### Pixel/RD53

- [RD53A manual](https://cds.cern.ch/record/2287593?ln=en)
- [RD53B (ITkPixV1) manual](http://cds.cern.ch/record/2665301)
- [Pixel readout chip review](https://arxiv.org/pdf/1705.10150.pdf)
- [RD53A/RD53B overview](https://drive.google.com/file/d/1HHQz6y28U7_AHb6EuqaDErdNbtzzpq_F/view?usp=sharing)
- [RD53B overview](https://drive.google.com/file/d/1TBEW9d_q_CeDe9XvyELFIrikDd2HewMT/view?usp=sharing)

### Yarr/DAQ

- [Documentation](https://yarr.web.cern.ch)

### Powerboards/Strips

- [Powerboard schematic](https://gitlab.cern.ch/theim/powerboard_v3/raw/master/barrel/pbv3_schematic.pdf)
- [Powerboard design status - Timon Heim](https://cernbox.cern.ch/index.php/s/zqxLoekXDJkjZHm)
- [Powerboard test flow - Evan Mladina](https://cernbox.cern.ch/index.php/s/OLSykNPmgVVkC5q)
- [Powerboard mass testing](https://cernbox.cern.ch/index.php/s/O28Yybu6EaAhmJt)
