---
title: "Resume"
date: 2021-04-20T09:35:19-07:00
draft: false
---

# Resume

------------------------------------------------

## Experience

* 2017 - present: **Research Scientist** - Lawrence Berkeley National Lab (Berkeley, USA)
* 2015 - 2017: **Postdoctoral fellow** - Lawrence Berkeley National Lab (Berkeley, USA)
* 2011 - 2015: **Wolfgang Gentner scholar** (PhD student) - CERN (Geneva, CH)
* 2009 - 2011: **Student assistent** - University of Wuppertal (Wuppertal, GER)

------------------------------------------------

## Education

* 2015: **Physics PhD** - University of Wuppertal (Germany)
    * Supervisors: *Prof. Dr. P. Maettig (Wuppertal)*, *Dr. H Pernegger (CERN)*
    * Thesis: Performance of the Insertable B-Layer for the ATLAS Pixel detector during Quality Assurance and a Novel Pixel detector readout concept based on PCIe ([CERN-THESIS-2016-085](https://cds.cern.ch/record/2206274))
* 2011: **Physics Diploma** - University of Wuppertal (Germany)
    * Supervisor: *Prof. Dr. P. Maettig (Wuppertal)*
    * Thesis: Design and development of the IBL-BOC firmware for the ATLAS Pixel IBL optical datalink system ([CERN-THESIS-2011-383](https://cds.cern.ch/record/2206280?ln=en))

------------------------------------------------

## Conference Talks

* **VERTEX 2020** (Virtual) - [First test results from the ITkPixV1 pixel readout chip](https://indico.cern.ch/event/895924/contributions/3968862/)
* **HSTD-12 2019** (Hiroshima, JP) - [Test Results from the RD53A Pixel readout chip and Design Status of its Successor](https://indico.cern.ch/event/803258/contributions/3582903/)
* **ACES 2018** (Geneva, CH) - [Overview of the Pixel Detectors Upgrades](https://indico.cern.ch/event/681247/contributions/2929051/)
* **TWEPP 2017** (Santa Cruz, USA) - [FE65-P2: A Prototype Pixel Readout Chip in 65nm Technology for HL-LHC Upgrades](https://indico.cern.ch/event/608587/contributions/2614082/)
* **CPAD 2016** (CalTech, USA) - [An SEU-immune self-tuned pixel chip architecture](https://indico.fnal.gov/event/24421/contributions/117391/)
* **IEEE 2016** (Strasbourg, FR) - FE65-P2: A Prototype Pixel Readout Chip in 65nm Technology for HL-LHC Upgrades
* **HSTD-9 2013** (Hiroshima, JP) - [Status and Performance of the ATLAS Pixel Detector after 3 Years of Operation](https://indico.cern.ch/event/228876/contributions/1539100/)

------------------------------------------------

## Selected Publications

* **2023**
    * *G. Calderini et al.* Qualification of the first pre-production 3D FBK sensors with ITkPixV1 readout chip - [[DOI](https://doi.org/10.22323/1.420.0025)]
    * *Z. Zhang et al.* Production and testing of the powerboard for ATLAS ITk Strip barrel modules - [[DOI](https://doi.org/10.1088/1748-0221/18/01/C01043)]
* **2022**
    * *G. Calderini et al.* Test of ITk 3D sensor pre-production modules with ITkPixV1.1 chip - [[DOI](https://doi.org/10.1088/1748-0221/18/01/C01010)]
* **2021**
    * *A. Blue et al.* Magnetic triggering — time-resolved characterisation of silicon strip modules in the presence of switching DC-DC converters - [[DOI](https://doi.org/10.1088/1748-0221/16/06/P06012)]
* **2020**:
    * *M. Backhaus et al.* - Study and parameter optimization of a tuning method for the online calibration of the RD53A readout chip - [[DOI](https://doi.org/10.1016/j.nima.2020.164594)]
    * *L. Poley et al.* - The ABC130 barrel module prototyping programme for the ATLAS strip tracker - [[DOI](https://doi.org/10.1088/1748-0221/15/09/P09004)]
    * *RD53 Collaboration* - RD53 analog front-end processors for the ATLAS and CMS experiments at the High-Luminosity LHC - [[DOI](https://doi.org/10.22323/1.373.0021)]
* **2019**:
    * *RD53 Collaboration* - Design implementation and test results of the RD53A, a 65 nm large scale chip for next generation pixel detectors at the HL-LHC - [[DOI](https://doi.org/10.1109/NSSMIC.2018.8824486)]
    * *P. Liu et al.* - Measured Effectiveness of Deep N-well Substrate Isolation in a 65nm Pixel Readout Chip Prototype - [[DOI](https://doi.org/10.1016/j.nima.2020.163842)]
    * *RD53 Collaboration* - Test results and prospects for RD53A, a large scale 65 nm CMOS chip for pixel readout at the HL-LHC - [[DOI](https://doi.org/10.1016/j.nima.2018.11.107)]
* **2018**:
    * *S. Marconi et al.* - Design implementation and test results of the RD53A, a 65 nm large scale chip for next generation pixel detectors at the HL-LHC - [[DOI](https://doi.org/10.1109/NSSMIC.2018.8824486)]
    * *ATLAS IBL Collaboration* - Production and Integration of the ATLAS Insertable B-Layer - [[DOI](https://doi.org/10.1088/1748-0221/13/05/T05008)]
    * *N. Whallon et al.* - Upgrade of the YARR DAQ system for the ATLAS Phase-II pixel detector readout chip - [[DOI](https://doi.org/10.22323/1.313.0076)]
    * *K. Dunne et al.* - Quad Module Hybrid Development for the ATLAS Pixel Layer Upgrade - [[DOI](https://doi.org/10.22323/1.313.0065)]
* **2017**:
    * *RD53 Collaboration* - Development of a Large Pixel Chip Demonstrator in RD53 for ATLAS and CMS Upgrades - [[DOI](https://doi.org/10.22323/1.313.0005)]
    * *T. Heim* - YARR - A PCIe based readout concept for current and future ATLAS Pixel modules - [[DOI](https://doi.org/10.1088/1742-6596/898/3/032053)]
    * *R. Carney et al.* - Results of FE65-P2 Pixel Readout Test Chip for High Luminosity LHC Upgrades - [[DOI](https://doi.org/10.22323/1.282.0272)]
    * *T. Heim et al.* - Self-Adjusting Threshold Mechanism for Pixel Detectors - [[DOI](https://doi.org/10.1016/j.nima.2017.06.040)]
    * *N. Whallon et al.* - Pixel readout chip software emulators for the YARR DAQ system upgrade - [[DOI](https://doi.org/10.1088/1742-6596/1085/3/032017)]
* **2014**:
    * *ATLAS IBL Collaboration* - ATLAS Pixel IBL: Stave Quality Assurance - [[ATL-INDET-PUB-2014-006](https://cds.cern.ch/record/1754509)]
    * *T. Heim* - Status and Performance of the ATLAS Pixel Detector after 3 Years of Operation - [[DOI](http://dx.doi.org/10.1016/j.nima.2014.04.058)]
* **2012**:
    * *ATLAS IBL Collaboration* - Prototype ATLAS IBL modules using the FE-I4A front-end readout chip [[DOI](http://dx.doi.org/10.1088/1748-0221/7/11/P11010)]
    * *G. Balbi et al.* - A PowerPC-based control system for the Read-Out-Driver module of the ATLAS IBL - [[DOI](http://dx.doi.org/10.1088/1748-0221/7/02/C02016)]
* **2011**:
    * *L. Ancu et al.* - The ATLAS IBL BOC Demonstrator - [[DOI](http://dx.doi.org/10.1109/NSSMIC.2011.6154423)]

