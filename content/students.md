---
title: "Student Projects"
date: 2021-04-20T09:35:19-07:00
draft: false
---

# Past student projects

* Chaitanya Bashyam (SULI, LBNL) - Powerboard production testing and test result analysis
* Hongjiang Kai (master student, UW) - YARR FW for SPEC optimization for multi-module setups & Aurora header-seaker
* Luc LePottier (grad student, UC Berkeley) - Magnetic trigger setup for ITk Strip modules and on-module Powerboard QC (qualification task)
* Zhirong Zhang (grad student, UC Berkeley) - Trigger logic for table top Pixel telescope
* Samantha Kelly (SULI, LBNL) - Powerobard test system interlock and production testing
* Taisei Kumakura (visiting grad student, Tsukuba) - ITkPiXv1 module characterisation
* Madeleine Leibovitch (BLUR, LBNL) - Identification of disconnected bumps using ML
* Noah Zisser (postbacc, LBNL) - Powerboard testing and commissioning of cooling crate
* Robin Xiong (grad student, UC Berkeley) - Xray machine commissioning and irradiation of RD53B chips (qualification task)
* Ryan Roberts (grad student, UC Berkeley) - Comparison of scan results from ITSDAQ and YARR software (qualification task)
* Maria Mironova (remote, grad student, University of Oxford (UK)) - Xray irradiation of ITkPixV1 chips
* Lucas Lopes Cendes (remote, bachelor honors thesis, UW) - [RD53B trigger generator for YARR FW](https://cds.cern.ch/record/2771675)
* Simon Huiberts (remote, grad student, University of Bergen (NO)) - Double injection scans on RD53A and ITkPixV1 chips
* Lauren Choquer (remote, master student, UW) - Aurora channel bonding implementation in YARR FW
* Evan Mladina (postbacc, LBNL) - Powerboard characterisation and setup of mass test system
* Gregory Ottino (grad student, UC Berkeley) - Irradiation of Powerboards and components (qualification task)
* Patrick McCormack (grad student) - [Absolute charge calibration of RD53A modules using compton scattering](https://arxiv.org/abs/2008.11860)
* Florent Stuessi (bachelor thesis, University of Fribourg (CH)) - Powerboard transient characterization and system testing
* Maic Queiroz (bachelor thesis, University of Fribourg (CH)) - Pixel data-stream aggregator
* Daniel Geisz (URAP, UC Berkeley) - Peltier based pixel module cooling system & Strip module readout
* Niharika Mittal (master thesis, UW) - RD53B data decoder
* Neha Sai Santpur (grad student, UC Berkeley) - Powerboard reliability testing (qualification task)
* Arisa Kubota (masther thesis, Tokyo Tech) - Local database implementation for ITk Pixel Modul QC
* Charilou Labitan (postbacc, LBNL) - RD53A wafer probing and testbeam analysis
* Lev Kurilenko (master thesis, UW) - Custom aurora 64b/66b decoder
* Nikola Whallon (SCGSR, UW) - SW emulator of RD53A/B chip for YARR
* Euchong Kim (master thesis, Tokyo Tech) - RD53A chip testing
* Adrien Stejer (URAP and BLUR, UC Berkeley) - Peltier based module cooling and FE-I4 based beam telescope
* Vyassa Baratham (grad student, UC Berkeley) - Trigger logic block for YARR FW
* Vincent Conus (bachelor thesis, University of Fribourg (CH)) - Development of run-time compression algorithms for calibration
* Dan Baumgartner (bachelor thesis, University of Fribourg (CH)) - Readout of ABC120 and HCC130 with YARR
* Arnaud Sautaux (master thesis, University of Fribourg (CH)) - Migration of YARR FW to Kintex FPGAs
* Andre He (URAP, UC Berkeley) - AMAC chip testing
* Katie Dunne (SULI, LBNL) - [FE65-P2 Chip testing](https://doi.org/10.22323/1.282.0272)
* Tobias Riedener (bachelor thesis, University of Fribourg (CH)) - Development of a Testbench for the new AMAC ASIC
* Donald Allum Jr (URAP, UC Berkeley) - Design of FE65-P2 singl chip card
