---
title: "Links"
date: 2021-04-20T09:35:19-07:00
draft: false
---

# Links

A collection of links I found useful, many might require a CERN or other login

## ITk

* [ITk Scripts](https://atlas-itk-scripts.web.cern.ch/atlas-itk-scripts/)

## LBNL - ATLAS group

* [Central Grafana](https://grafana.physics.lbl.gov:3000)
* [ATLAS Wiki](https://atlaswiki.lbl.gov)

## LBNL - General

* [LBL IP lookup](https://onestop.lbl.gov)
* [LBL Syslog](https://security.lbl.gov/syslog/recent)

## CERN

* [My Twiki](https://twiki.cern.ch/twiki/bin/view/Main/TimonHeim)


