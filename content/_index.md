---
title: "Index"
date: 2021-04-20T09:43:28-07:00
draft: false
---

![mixup](images/collage.jpg)

# Research Activities

I specialize in detector instrumentation for particle physics, specifically silicon tracking detectors for the [ATLAS experiment](https://atlas.cern) at the [CERN Large Hadron Collider (LHC)](https://home.cern). I'm currently involved in the R&D and production of detector parts of the Inner Tracker (ITk) [Pixel](https://cds.cern.ch/record/2285585?ln=en) and [Strip](https://cds.cern.ch/record/2257755?ln=en) detector, which is the all-silicon replacement of the current ATLAS Inner detector.

I'm also part of the RD53 collaboration, which is tasked with designing the pixel detector readout chips for the next generation Pixel detectors of the ATLAS and CMS detectors.

More details can be found [here]({{< ref "research.md" >}}).

My general research interests include:
* Solid-state tracking detector instrumentation
* Silicon sensor technology
* IC design for particle physics
* Data aquisition for particle detectors (hardware, firmware, software)
* Usage of FPGAs for data processing and data aquisition
* Detector system design

--------------------------------------------------------------

# Student Projects

## UC Berkeley and US based students

If you are interested in working with our group on instrumentation for particle physics, check out the various [internship possibiltites at LBNL](https://education.lbl.gov/internships/), [URAP projects](https://urapprojects.berkeley.edu/projects/detail.php?id_list=Phy0997) or feel free to reach out to me for more information. If you want to get an idea about what kind of projects we have, have a look [here]({{< ref "students.md" >}}) for past student projects.

## International Visitors

We are happy to look into the possibility to host international visitors (students, but also more senior folks) for extended stays at LBNL to work with us on related research activities. Please reach out to us to discuss financial, visa, or housing aspects.
