---
title: "Research"
date: 2021-04-20T09:35:19-07:00
draft: false
---

# Current research projects

## High time precision front-end technology in 28nm CMOS 

![28nm prototype ASIC](../images/bigrock.jpg)

To meet the requirements set for detector systems at future colliders we are actively investigating smaller than 65nm CMOS technologies for the basis of our readout chips. The next smaller technology is 28nm and we are designing prototype ASICs exploring specific aspects which could pave the way towards the next Pixel detector system. Considering this technology is also new to the LBNL IC design team we have started a broader R&D effort to understand its capabilities within the scientific
domain, specifically for the usage in extreme radiation (HEP collider) or cryogenic environments (very low noise for rare interactions)

* 4D tracking or precision timing analog front-end (O(100ps) or less)
* High resolution TDC (~5ps) for characterization of precision timing front-ends
* High speed data tranmission over low mass cables using PAM-4 encoding
* Low noise analog front-end capable of reading out high input cpacitance detectors (e.g. Ge or SiPM)
* Voltage reference
* Low resistance multiplexer

----------------------------------------------------------------

## Design and characterization of ITkPixV1/V2 Pixel detector readout chip

![ITkPixV1 chip mounted on Single Chip Card](../images/rd53b.jpg)

ITkPixV1 is designed by the RD53 collaboration and the successor of the RD53A demoinstrator chip. It's the pre-production chip for the ITk Pixel detector and is being characterized for the given requirements to operate within the harsh environment of the HL-LHC. Each ITkPixV1 chip features a 400x384 pixel matrix with 50um by 50um pitch. Each pixel is designed for low power low threshold operation, typically being tuned to a 1000e threshold while consuming less than 8uA per pixel (analog + digital). This is achieved in part by the differential analog front-end, which was designed here at LBNL. The overall chip architecture is meant to process up to 3 GHz/cm2 of particle hits, resulting in readout speeds up to 5.12 Gbps (facilitated by four 1.28Gbps data outputs).

References:
* RD53 collaboration website - [[link](https://rd53.web.cern.ch)]
* RD53B Testing Twiki - [[internal link](https://twiki.cern.ch/twiki/bin/view/RD53/RD53BTesting)]
* RD53B Manual - [[link](https://twiki.cern.ch/twiki/bin/view/RD53/RD53BTesting)]

----------------------------------------------------------------

## System level design and testing for the ITk Pixel detector

![ITk Pixel Quad and Triplet module](../images/modules.jpg)

The ITk Pixel detector will use two types of modules: a quad module, which integrates a single planar sensor tile, 4 ITkPix readout chips, and one flex PCB into one module, and a triplet module, which is made up of 3 single chip assemblies with 3D sensors combined into one module via a flex PCB. To minimize mass introduced by electrical services, the modules will be operated in a serial power chain.
Serial powering is enabled by the Shunt-LDO circuit within the ITkPix readout chip, which fixes the current consumption of the readout chip at a constant level. The ITk Pixel detector will be the first large system using a serially powered system and hence testing of serial power chains is important to understand the relaibility of the system.

References:
* ITk Pixel Technical Design Report - [[link](https://cds.cern.ch/record/2285585?ln=en)]

----------------------------------------------------------------

## Design and production of the ITk Strip Barrel Module Powerboard

![Powerboards on test carrier](../images/powerboard.jpg)

In order to reduce the mass of the ITk Strip detector a powering scheme was chosen which distributes a higher voltage (11V) to each module on a support structure in parallel and performs DCDC conversion locally on each module. This functionality is facilitated on the Powerboard by means of CERN developed DCDC regulator IC, the bPOL12V.
In addition the Powerboard also serves as a local monitoring and control hub of the module, using the Analog Monitor And Control chip (AMAC) designed by University of Pennsylvania, and can also switch the HV bias to sensor on and off using a GaN FET as a switch.

In total LBNL will produce 15,000 Powerboards to supply all ITk Strip Barrel Module production sites. An important aspects of this production is the electrical qualification of each Powerboard over the course of the 2-year production time, which will start around the beginning of 2023.

References:
* ITk Strips Technical Design Report - [[link](https://cds.cern.ch/record/2257755?ln=en)]
* Powerboard design repository - [[gitlab](https://gitlab.cern.ch/theim/powerboard_v3/)]

----------------------------------------------------------------

## YARR - A readout solution for the ITk system

![YARR logo](../images/yarr.jpg)

YARR (Yet Another Rapid Readout) is a software package whose goal is to provide a hardware agnostic framework to perform pixel and strip readout chip operation and calibration. YARR started out as a calibration software for pixel readout chips like FE-I4, but has since been generalised and expanded to also serve FE65-P2, RD53A, ITkPixV1, ABCStar, and HCCStar.
It's software-centric approach, made possible by modern CPU architectures, is well suited for the operation of readout chips with FELIX, the DAQ hardware chosen for the readout of the ITk detector. Other hardware platoforms better suited to smaller scale lab operation also exist, with the baseline being commercially available FPGA PCIe card which serves as a reprogrammable interface for the custom detector solution.
The firmware for these cards is being developed at LBNL and these system will be used during the ITk Pixel module production for quality control. It's also the system of choice for characterisation of the latest generation of readout chips.

References:
* YARR SW repository - [[gitlab](https://gitlab.cern.ch/YARR/YARR)]
* YARR PCIe FW repository - [[gitlab](https://gitlab.cern.ch/YARR/YARR-FW)]
* YARR documentation - [[link](http://yarr.web.cern.ch)]

----------------------------------------------------------------

# Past research projects

* Pixel detector readout chip design and testing:
    * RD53A chip characterisation leading to choice of analog front-end for CMS and ATLAS pre-production chips (CROCv1 and ITkPixV1)
    * FE65-P2 prototype chip testing
    * FE-I4 module testing during final ATLAS Pixel IBL stave testing at CERN
* Pixel detector testing
    * Final electrical qualification of ATLAS Pixel IBL staves before integration
    * Testing of opto-converter plugins for the ATLAS Pixel detector
    * Commissioning of the ATLAS Pixel IBL system test at CERN
* DAQ development
    * Design of the ATLAS Pixel IBL BOC firmware


